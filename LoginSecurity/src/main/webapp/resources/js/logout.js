/**
 * 로그아웃
 */

function logout() {
	$.ajax({
		type: "GET",
		url: "/login/logout.do",
		success: function() {
			alert("Spring Security 로그아웃 완료");
			window.location = '/';
		},
		error: function(request, error) {
			alert('code:' + request.status + '\n' + 'message:' + request.responseText + '\n' + 'error:' + error);
		}
	});
}

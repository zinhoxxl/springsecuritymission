<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script type="text/javascript" src="${path}/resources/js/logout.js"></script>
<html>
<head>
<title>Home</title>
</head>
<body>
	<div class="container" align="center">
		<input onclick="logout();" type="button"
			class="btn btn-primary" style="margin: 50px 0 0 190px;" value="logout" />
		<h1>메인 페이지</h1>
		<span style="margin-right: 6px; padding-bottom: 10px;">사용자 이름</span><input
			type="text" name="username" class="form-control-right" id="username" value="${username }" readonly="readonly">
		<br> <span style="margin-right: 50px;">회사</span><input
			type="text" name="cname" class="form-control-right" id="userpw" value="${cname }" readonly="readonly">
	</div>

</body>
</html>

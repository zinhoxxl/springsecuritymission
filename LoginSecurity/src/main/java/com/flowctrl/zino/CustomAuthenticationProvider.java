package com.flowctrl.zino;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

import com.flowctrl.zino.login.dao.LoginDAO;
import com.flowctrl.zino.login.dto.CustomUserDetails;
import com.flowctrl.zino.login.service.Sha256;

public class CustomAuthenticationProvider implements AuthenticationProvider {

	@Autowired
	private LoginDAO loginDao;
	
	
	@Autowired
	private CustomUserDetailsService userDetailsService;

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		
		String userid   = (String) authentication.getPrincipal();
		String password = (String) authentication.getCredentials();
		
		String encryptSHA256 = Sha256.testSHA256(password);
		CustomUserDetails userDetails = (CustomUserDetails) userDetailsService.loadUserByUsername(userid);
		String signPassword = userDetails.getPassword();
		
		if (!encryptSHA256.equals(signPassword)) {
			throw new BadCredentialsException(userid);
		}
		Authentication newAuth = new UsernamePasswordAuthenticationToken(userid, encryptSHA256);
		return newAuth;
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}

}

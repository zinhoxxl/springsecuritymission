package com.flowctrl.zino;


import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.flowctrl.zino.login.dao.LoginDAO;
import com.flowctrl.zino.login.dto.CustomUserDetails;

public class CustomUserDetailsService implements UserDetailsService {
	
	@Autowired
	private LoginDAO loginDao;
	
	@Override
	public UserDetails loadUserByUsername(String userid) throws UsernameNotFoundException {

		Map<String, Object> map = loginDao.getUserDetail(userid);

		String username  = (String) map.get("username");
		String password  = (String) map.get("PASSWORD");
		String enabled   = (String) map.get("enabled");
		String authority = (String) map.get("authority");

		CustomUserDetails userDetail = new CustomUserDetails();

		userDetail.setUsername(username);
		userDetail.setPASSWORD(password);
		userDetail.setENABLED(enabled);
		userDetail.setAUTHORITY(authority);

		return userDetail;
	}
}

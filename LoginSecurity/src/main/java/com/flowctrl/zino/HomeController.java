package com.flowctrl.zino;

import java.io.IOException;
import java.security.Principal;
import java.util.Locale;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.flowctrl.zino.login.dto.LoginVO;
import com.flowctrl.zino.login.service.LoginService;

@Controller
public class HomeController {

	@Autowired
	LoginService loginService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model, Principal principal, Authentication authentication, HttpServletResponse response) throws IOException {
		
		LoginVO loginVO = new LoginVO();
		loginVO.setUserid(principal.getName());
		loginVO = loginService.getInfo(loginVO);

		model.addAttribute("cname", loginVO.getCname());
		model.addAttribute("username", loginVO.getUsername());

		return "home";
	}

}

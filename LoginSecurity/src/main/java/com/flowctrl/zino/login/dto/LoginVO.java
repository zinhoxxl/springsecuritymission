package com.flowctrl.zino.login.dto;

import lombok.Data;

@Data
public class LoginVO {

	private String userid;
	private String password;
	private String cname;
	private String username;
	private String usergrade;

}

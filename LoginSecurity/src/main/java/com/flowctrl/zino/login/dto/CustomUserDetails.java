package com.flowctrl.zino.login.dto;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class CustomUserDetails implements UserDetails {
	
	private String USERNAME;
	private String PASSWORD;
	private String AUTHORITY; 
	private String ENABLED; 

	
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		ArrayList<GrantedAuthority> auth = new ArrayList<GrantedAuthority>();
		auth.add(new SimpleGrantedAuthority(this.AUTHORITY));

		return auth;
	}

	
	@Override
	public String getPassword() {
		return PASSWORD;
	}

	public void setPASSWORD(String pASSWORD) {
		this.PASSWORD = pASSWORD;
	}

	public void setAUTHORITY(String aUTHORITY) {
		this.AUTHORITY = aUTHORITY;
	}

	
	@Override
	public boolean isEnabled() {
		System.out.println(ENABLED);
		return "1".equals(ENABLED) ? true : false;
	}

	public void setENABLED(String eNABLED) {
		this.ENABLED = eNABLED;
	}

	
	@Override
	public String getUsername() {
		return USERNAME;
	}

	public void setUsername(String uSERNAME) {
		this.USERNAME = uSERNAME;
	}

	
	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	
	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	
	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

}

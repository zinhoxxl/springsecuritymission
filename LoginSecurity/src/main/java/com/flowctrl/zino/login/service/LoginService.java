package com.flowctrl.zino.login.service;


import com.flowctrl.zino.login.dto.LoginVO;

public interface LoginService {
	
	public LoginVO getInfo(LoginVO loginVO);

}

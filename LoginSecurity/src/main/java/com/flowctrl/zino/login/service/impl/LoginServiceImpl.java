package com.flowctrl.zino.login.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.flowctrl.zino.login.dao.LoginDAO;
import com.flowctrl.zino.login.dto.LoginVO;
import com.flowctrl.zino.login.service.LoginService;

@Repository
public class LoginServiceImpl implements LoginService{
	
		@Autowired
		private LoginDAO loginDAO;
		
		@Override
		public LoginVO getInfo(LoginVO loginVO){		
			return loginDAO.getInfo(loginVO);
		}

}

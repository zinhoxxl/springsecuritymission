package com.flowctrl.zino.login.dao;

import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.flowctrl.zino.login.dto.LoginVO;

@Repository("LoginDAO")
public class LoginDAO {

	@Autowired
	private SqlSession mybatis;

	public LoginVO getInfo(LoginVO loginVO) {
		return mybatis.selectOne("login.getInfo", loginVO);
	}

	public Map<String, Object> getUserDetail(String userid) {
		return mybatis.selectOne("login.getUserDetail", userid);
	}

}

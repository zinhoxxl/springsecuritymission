package com.flowctrl.zino.login.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.flowctrl.zino.login.service.LoginService;

@Controller
public class LoginController {
	
	@Autowired
	LoginService loginService;

	@RequestMapping(value="/login/loginPage.do") 
	public String loginPage() { 
		return "/login/loginPage"; 
	}
	
	@RequestMapping(value="/login/accessDenied.do") 
	public String accessDeniedPage() throws Exception { 
		return "/login/accessDenied"; 
	}
	
	@RequestMapping(value="/login/logout.do") 
	public String logout() { 
		return "/login/loginPage"; 
	}
	
}


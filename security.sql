create user security IDENTIFIED by security;
GRANT dba, resource,connect to security;

CREATE TABLE TEST_USER (
    USERID varchar2(100) primary key,
    PASSWORD varchar2(500),
    CNAME varchar2(100),
    USERNAME varchar2(100),
    USERGRADE varchar2(100),
    enabled varchar2(100)
);
insert into TEST_USER values ('guest1', 'abcd1234', 'flow control', '게스트1', '02', '1');
insert into TEST_USER values ('guest2', 'abcd1234', 'flow control', '게스트2', '02', '1');
insert into TEST_USER values ('guest3', 'abcd1234', 'flow control', '게스트3', '0', '0');
insert into TEST_USER values ('admin', 'admin1234', 'flow control', '관리자', '01', '1');

commit;
